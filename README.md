# Cleaning-Data

Development of Packt book

The contents of this repository are copyright of Packt Publishing, 
and may not be reproduced without prior written permission.  Review
and evaluation of this repository content is welcomed by it author,
but only local checkouts for personal use are permitted.

## Notes from reviewers (thanks Micah Dubinko):

### Conda approach

(Highly recommended) Here's how to start fresh:

Install Python 3.8+ Anaconda from https://www.anaconda.com/products/individual then

```bash
% conda update conda
% conda init zsh  # adjust to taste
% conda env create --file environment.yml
% conda activate cleaning
% R -f setup.R
```

## Other tools

### PostgreSQL

### Node

### Spark


### Mac homebrew

Micah: I had to follow these instructions to get Python 3.8 working:
https://stackoverflow.com/questions/60453261/how-to-default-python3-8-on-my-mac-using-homebrew
(maybe I should bite the bullet and to with conda...)

